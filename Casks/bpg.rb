cask 'bpg' do
  version '1.5'
  sha256 'cf1f974e8d8ad1a331db1779864621a2cd72b5eb7d80298226ae62629068b19f'
  url "https://gitlab.com/Myl0g/#{token}_gui/uploads/080fd5fa629c6cbd8354a1215cf04a61/bpg-#{version}.zip"
  name 'bpg'
  homepage 'https://gitlab.com/myl0g/bpg_gui'

  depends_on formula: 'gnupg'
  depends_on formula: 'pinentry-mac'

  app 'bpg.app'

  caveats <<~EOS
    You must run the following commands to complete installation of pinentry-mac:

    echo "pinentry-program /usr/local/bin/pinentry-mac" >> ~/.gnupg/gpg-agent.conf
    killall gpg-agent
  EOS
end
