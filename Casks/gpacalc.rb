cask 'gpacalc' do
  version '1.7'
  sha256 :no_check

  # latest.gpa.milogilad.com was first verified as official when added to the cask
  url 'latest.gpa.milogilad.com'
  name 'gpacalc'
  homepage 'https://gitlab.com/myl0g/gpacalc_gui'

  app 'gpacalc.app'
end
