class Bank < Formula
    desc "Simple financial logbook CLI"
    homepage "https://gitlab.com/myl0g/bank"
    url "https://gitlab.com/Myl0g/bank/uploads/91e9f0ea28e56e2521cb1d84377eef0e/bank-darwin.tar.xz"
    version "1.7.0"
    sha256 "2ba8ec91ae5d7aedfdb3beb0699c606218ed60fc12ab5e87b4f411759c355c1e"
  
    def install
      mkdir "#{bin}"
      cp "bank-darwin", "#{bin}/bank"
    end
  
    test do
      system "bank", "help"
    end
  end
